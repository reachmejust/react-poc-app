import React from 'react';
import axios from 'axios';

import User from './../components/User';

class Users extends React.Component {

    state = {
        users: []
    }

    componentDidMount = () => {
        axios.get('https://jsonplaceholder.typicode.com/users').then((response)=>{
            console.log(response.data);
            this.setState({users: response.data});
          },(error)=>{
            console.log(error);
          });
    }

    render() {
        let users = 'Loading...';
        if(this.state.users.length > 0){
            users = this.state.users.map((user)=>{
            return <User key={user.name} name={user.name}/>;
            });
        }
        return users;
    }

}

export default Users;