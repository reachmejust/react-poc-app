import React from 'react';


class Toggle extends React.Component {

    constructor(props) {
        super(props);
        this.state = {isToggleOn: true, fruits: ['Apple','Banana', 'Custard Apple']}
//        this.handleToggle = this.handleToggle.bind(this);
    }

    // handleToggle(e) {
    //     console.log(e.target);
    //     this.setState(prevState => ({isToggleOn:!prevState.isToggleOn}));
    // }


    handleToggle = (e) => {
        console.log(e);
        this.setState(prevState => ({isToggleOn:!prevState.isToggleOn}));
    }

    render() {

        const fruits = this.state.fruits.map(f=>{
            return <li key={f}>{f}</li>;
        });

        return (
            <div>
            <ul>
            {fruits}</ul>
            <button onClick={this.handleToggle}>{this.state.isToggleOn?'ON':'OFF'}</button>
            </div>
        );
    }
}

export default Toggle;