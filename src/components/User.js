import React from 'react';
import './User.css';

const user = (props) => {
    return (
        <div className='user'>{props.name}</div>
    );
}

export default user;