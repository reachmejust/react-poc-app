import React from 'react';
import './Order.css';

class Order extends React.Component {

    state = {
        problems: [],
        detail: [],
        reasons: [],
        resolutions: []
    }

    componentDidMount = () => {
        console.log('componentDidMount called');
        var probs = [];
        for(var i = 1; i <= 10; i++) {
            probs.push({code:i,name:'B'+i}) ;         
        }
        this.setState({problems: probs});
    }
    problemSelected = (p) => {
        var details = [];
        for(var i = 1; i <= 10; i++) {
            details.push({code:i,name:'D'+p.code+i}) ;         
        }
        this.setState({...this.state.problems, detail:details, reasons: [], resolutions: []});   
    }
    detailSelected = (d) => {
        var reasons = [];
        for(var i = 1; i <= 10; i++) {
            reasons.push({code:i,name:'R'+d.code+i}) ;         
        }
        this.setState({...this.state.problems, ...this.state.detail, reasons: reasons, resolutions: []});
    }
    reasonSelected = (r) => {
        var suggestions = [];
        for(var i = 1; i <= 10; i++) {
            suggestions.push({code:i,name:'S'+r.code+i}) ;         
        }
        this.setState({...this.state.problems, ...this.state.detail, ...this.state.reasons, resolutions: suggestions});
    }

    render() {
        const probElems = this.state.problems.map((p)=>{
            return <li key={p.code} onClick={()=>this.problemSelected(p)}>{p.name}</li>
        });
        const detailElems = this.state.detail.map((d)=>{
            return <li key={d.code} onClick={()=>this.detailSelected(d)}>{d.name}</li>
        });
        const reasonElems = this.state.reasons.map((r)=>{
            return <li key={r.code} onClick={()=>this.reasonSelected(r)}>{r.name}</li>
        });
        const resolutionElems = this.state.resolutions.map((s)=>{
            return <li key={s.code}>{s.name}</li>
        });

        return <div className="container">
            <div className="row">
                <div className="col-md-12 head">
                    <label>Recipient</label><span> RECIPIENT NAME</span>
                </div>
            </div>
            <div className="row">
                <div className="col-md-3">
                    <span>Problem Code</span>
                    <ul className="scroll">
                    {probElems}
                    </ul>
                </div>
                <div className="col-md-3">
                    <span>Detail Code</span>
                    <ul className="scroll">
                    {detailElems}
                    </ul>
                </div>
                <div className="col-md-3">
                    <span>Reasons</span>
                    <ul className="scroll">
                    {reasonElems}
                    </ul>
                </div>
                <div className="col-md-3">
                    <span>Resolutions</span>
                    <ul className="scroll">
                    {resolutionElems}
                    </ul>
                </div>
            </div>
            <div className="row">
                <div className="col-md-6">
                    
                        <div className="row">
                            <div className="col-md-4">
                                <span>Status</span>
                                <ul className="scroll">
                                    <li>STATUS_CODE</li>    
                                </ul>
                            </div>
                            <div className="col-md-4">
                                <span>Fault Code</span>
                                <ul className="scroll">
                                    <li>FLT_CODE</li>    
                                </ul>
                            </div>
                            <div className="col-md-4">
                                <span>Error Code</span>
                                <ul className="scroll">
                                    <li>ERROR_CODE</li>    
                                </ul>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12">
                            <span>Options</span><br/>
                            
                                <button type="button" className="btn">Resend Order</button>
                                <button type="button" className="btn">Replace Order</button>
                                <button type="button" className="btn">Duplicate Order</button>
                                <button type="button" className="btn">Change Order</button>
                                <button type="button" className="btn">Fill Order</button>
                                <button type="button" className="btn">Select New Florist</button>
                                <button type="button" className="btn">Bloomnet Form</button>
                            
                            </div>
                        </div>
                    
                </div>
                <div className="col-md-6">

                </div>
            </div>
            <div className="row">
                <div className="col-md-12 actions">
                    <button type="button" className="btn btn-primary">Continue</button>
                    <button type="button" className="btn">Back</button>
                </div>
            </div>
        </div>;
    }

}

export default Order;